package com.example.controller;


import com.example.entity.ProductPricing;
import com.example.service.ProductPricingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/productpricing")
public class ProductPricingController {

    @Autowired
    private ProductPricingService productPricingService;

    @GetMapping("")
    public Iterable<ProductPricing> getAllProductPricing() {
        return productPricingService.findAll();
    }

    @GetMapping("/{id}")
    public Optional<ProductPricing> searchProductPricing(@PathVariable int id) {
        return productPricingService.findById(id);
    }

    @PostMapping("")
    public void addProductPricing(@RequestBody ProductPricing productPricing) {
        productPricingService.insert(productPricing);
    }

    @PutMapping("/{id}")
    public void updateProductPricing(@RequestBody ProductPricing productPricing) {
        productPricingService.updateProductPricing(productPricing);
    }

    @DeleteMapping("/{id}")
    public void deleteProductPricing(@RequestBody ProductPricing productPricing) {
        productPricingService.deleteProductPricing(productPricing);
    }

}
