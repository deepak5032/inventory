package com.example.controller;

import com.example.entity.Pricing;
import com.example.entity.TheLogConverter;
import com.example.service.PricingLogService;
import com.example.service.PricingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/categories/{id}/products/{id}/pricings")
public class PricingController {


    @Autowired
    public PricingService pricingService;
    @Autowired
    private PricingLogService pricingLogService;

    @GetMapping("")
    public Iterable<Pricing> getAllPricing() {
        return pricingService.findAll();
    }


    @GetMapping("/{id}")
    public Optional<Pricing> searchPricing(@PathVariable int id) {
        return pricingService.find(id);
    }

    @PostMapping("")
    public void addPricing(@RequestBody Pricing pricing) {
        pricingService.insert(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }

    @PutMapping("/{id}")
    public void updateCategory(@RequestBody Pricing pricing) {
        pricingService.updatePricing(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }

    @DeleteMapping("/{id}")
    public void deletePricing(@RequestBody Pricing pricing) {
        pricingService.deletePricing(pricing);
        pricingLogService.insert(TheLogConverter.pricingLogLogConverter(pricing));
    }


}

