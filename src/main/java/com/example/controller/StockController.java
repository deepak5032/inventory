package com.example.controller;

import com.example.entity.Stock;
import com.example.entity.TheLogConverter;
import com.example.service.StockLogService;
import com.example.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/stocks")
public class StockController {
    @Autowired
    private StockService stockService;
    @Autowired
    private StockLogService stockLogService;

    @GetMapping("")
    public Iterable<Stock> getAllStock() {
        return stockService.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Stock> searchStock(@PathVariable int id) {
        return stockService.findById(id);
    }

    @PostMapping("")
    public void addStock(@RequestBody Stock stock) {
        stockService.insert(stock);
        stockLogService.insert(TheLogConverter.stockLogConverter(stock));
    }

    @PutMapping("/{id}")
    public void updateStock(@RequestBody Stock stock) {
        stockService.updateStock(stock);
        stockLogService.insert(TheLogConverter.stockLogConverter(stock));
    }

    @DeleteMapping("/{id}")
    public void deleteStock(@RequestBody Stock stock) {
        stockService.deleteStock(stock);
        stockLogService.insert(TheLogConverter.stockLogConverter(stock));
    }

}
