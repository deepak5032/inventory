package com.example.controller;

import com.example.entity.ProductInvoice;
import com.example.service.ProductInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/productinvoice")
public class ProductInvoiceController {
    @Autowired
    private ProductInvoiceService productInvoiceService;

    @GetMapping("")
    public Iterable<ProductInvoice> getAllProductInvoice() {
        return productInvoiceService.findAll();
    }

    @GetMapping("/{id}")
    public Optional<ProductInvoice> searchProductInvoice(@PathVariable int id) {
        return productInvoiceService.findById(id);
    }

    @PostMapping("")
    public void addProductInvoice(@RequestBody ProductInvoice productInvoice) {
        productInvoiceService.insert(productInvoice);
    }

    @PutMapping("/{id}")
    public void updateProductInvoice(@RequestBody ProductInvoice productInvoice) {
        productInvoiceService.updateProductInvoice(productInvoice);
    }

    @DeleteMapping("/{id}")
    public void deleteProductInvoice(@RequestBody ProductInvoice productInvoice) {
        productInvoiceService.deleteProductInvoice(productInvoice);
    }

}
